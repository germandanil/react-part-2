import Chat from "./src/components/chat";
import rootReducer from "./src/reducers/index";

export default {
  Chat,
  rootReducer,
};
