import {
  PRELOADER_OFF,
  SET_MESSAGES,
  DELETE_MESSAGE,
  OPEN_EDITOR,
  CLOSE_EDITOR,
  SAVE_EDIT,
  ADD_MESSAGE,
} from "./actionTypes";
import { MY_ID, MY_NAME, MY_SRC_AVA } from "../myConfig";

export const preloaderOff = () => {
  return {
    type: PRELOADER_OFF,
  };
};
export const setMessages = (messagesArray) => {
  return {
    type: SET_MESSAGES,
    payload: {
      messages: messagesArray,
    },
  };
};
export const addMessage = (TEXT, dateStr) => {
  return {
    type: ADD_MESSAGE,
    payload: {
      name: MY_NAME,
      avatar: MY_SRC_AVA,
      id: MY_ID,
      text: TEXT,
      createdAt: dateStr,
    },
  };
};
export const deleteMessage = (ID) => {
  return {
    type: DELETE_MESSAGE,
    payload: {
      id: ID,
    },
  };
};
export const openEditor = (ID, text) => {
  return {
    type: OPEN_EDITOR,
    payload: {
      selectedID: ID,
      startText: text,
    },
  };
};

export const closeEdit = () => {
  return {
    type: CLOSE_EDITOR,
  };
};

export const saveEdit = (text, dateStr) => {
  return {
    type: SAVE_EDIT,
    payload: {
      editText: text,
      editDate: dateStr,
    },
  };
};
