export const PRELOADER_OFF = "PRELOADER_OFF";
export const SET_MESSAGES = "SET_MESSAGES";
export const DELETE_MESSAGE = "DELETE_MESSAGE";
export const OPEN_EDITOR = "OPEN_EDITOR";
export const CLOSE_EDITOR = "CLOSE_EDITOR";
export const SAVE_EDIT = "SAVE_EDIT";
export const ADD_MESSAGE = "ADD_MESSAGE";
