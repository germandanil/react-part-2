import {
  PRELOADER_OFF,
  SET_MESSAGES,
  DELETE_MESSAGE,
  OPEN_EDITOR,
  SAVE_EDIT,
  CLOSE_EDITOR,
  ADD_MESSAGE,
} from "./actionTypes";
import { MY_ID, MY_NAME, MY_SRC_AVA } from "../myConfig";
import generatorId from "../generatorId/generatorId";
const _ = require("lodash");

const initialState = {
  chat: {
    messages: [],
    editModal: false,
    selectedEditID: "",
    startEditText: "",
    preloader: true,
  },
};

export default function chat(state = initialState, action) {
  switch (action.type) {
    case PRELOADER_OFF: {
      return {
        chat: {
          ...state.chat,
          preloader: false,
        },
      };
    }
    case SET_MESSAGES: {
      return {
        chat: {
          ...state.chat,
          messages: action.payload.messages,
        },
      };
    }
    case ADD_MESSAGE: {
      let newMessages = [...state.chat.messages];
      newMessages = newMessages.map((message) => Object.assign({}, message));
      const message = {
        userId: MY_ID,
        id: generatorId(),
        avatar: MY_SRC_AVA,
        name: MY_NAME,
        text: action.payload.text,
        createdAt: action.payload.createdAt,
      };
      newMessages.push(message);
      return {
        chat: {
          ...state.chat,
          messages: newMessages,
        },
      };
    }
    case DELETE_MESSAGE: {
      const newMessages = state.chat.messages.filter(
        (m) => m.id !== action.payload.id
      );
      return {
        chat: {
          ...state.chat,
          messages: newMessages,
        },
      };
    }
    case OPEN_EDITOR: {
      const obj = {
        chat: {
          ...state.chat,
          editModal: true,
          selectedEditID: action.payload.selectedID,
          startEditText: action.payload.startText,
        },
      };
      return obj;
    }
    case CLOSE_EDITOR: {
      return {
        chat: {
          ...state.chat,
          editModal: false,
          selectedEditID: "",
          startEditText: "",
        },
      };
    }
    case SAVE_EDIT: {
      let newMessages = [...state.chat.messages];
      newMessages = newMessages.map((message) => Object.assign({}, message));

      const editMessage = _.find(newMessages, {
        id: state.chat.selectedEditID,
      });
      editMessage.text = action.payload.editText;
      editMessage.editedAt = action.payload.editDate;
      return {
        chat: {
          ...state.chat,
          messages: newMessages,
          editModal: false,
          selectedEditID: "",
          startEditText: "",
        },
      };
    }
    default:
      return state;
  }
}
