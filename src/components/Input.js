import React from "react";
import { connect } from "react-redux";
import { addMessage } from "../chatReducer/actions";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
  }
  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  handleEnter(event) {
    if (event.key === "Enter") this.handleSubmit(event);
  }
  handleSubmit(event) {
    const date = new Date().toString();
    if (this.state.value !== "") {
      this.props.addMessage(this.state.value, date);
      this.setState({ value: "" });
    }

    setTimeout(() => {
      const element = document.querySelector(".message-list");
      if (element) {
        element.scroll({
          top: element.scrollHeight,
          behavior: "smooth",
        });
      }
    }, 200);

    event.preventDefault();
  }

  render() {
    return (
      <div className="message-input">
        <form onSubmit={this.handleSubmit}>
          <label>
            <textarea
              className="message-input-text"
              value={this.state.value}
              onChange={this.handleChange}
              onKeyDown={this.handleEnter}
            >
              {this.props.startText}
            </textarea>
          </label>
          <input
            className="message-input-button"
            type="submit"
            value={"Send"}
          />
        </form>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    addMessage: (TEXT, dateStr) => {
      dispatch(addMessage(TEXT, dateStr));
    },
  };
};

export default connect(null, mapDispatchToProps)(MessageInput);
