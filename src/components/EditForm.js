import React from "react";
import { connect } from "react-redux";

import { closeEdit, saveEdit } from "../chatReducer/actions";

class EditForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.startEditText !== this.props.startEditText) {
      this.setState({ value: nextProps.startEditText });
    }
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  handleEnter(event) {
    if (event.key === "Enter") this.handleSubmit(event);
  }
  handleSubmit(event) {
    const date = new Date().toString();
    if (this.state.value === this.props.startEditText) this.handleCancel();
    else this.props.saveEdit(this.state.value, date);
    event.preventDefault();
  }
  handleCancel() {
    this.props.closeEdit();
  }

  render() {
    let modalClassName = "edit-message-modal";
    if (this.props.show) modalClassName += " modal-shown";
    return (
      <div className={modalClassName}>
        <div className="edit-message-modal-body">
          <div className="close-btn-wrp">
            <button className="edit-message-close" onClick={this.handleCancel}>
              <img src="./close.png" alt="close button" />
            </button>
          </div>
          <form onSubmit={this.handleSubmit}>
            <label>
              <textarea
                className="edit-message-input"
                value={this.state.value}
                onChange={this.handleChange}
                onKeyDown={this.handleEnter}
              >
                {this.props.startText}
              </textarea>
            </label>
            <div className="btn-save-wrp">
              <input
                className="edit-message-button"
                type="submit"
                value="Save"
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    startEditText: state.chat.startEditText,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    closeEdit: () => dispatch(closeEdit()),
    saveEdit: (text, dateStr) => dispatch(saveEdit(text, dateStr)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);
