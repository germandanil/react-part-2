import React from "react";
import { connect } from "react-redux";
import { deleteMessage, openEditor } from "../chatReducer/actions";
class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLiked: false,
    };
    this.onClick = this.onClick.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.selectMessageForEdit = this.selectMessageForEdit.bind(this);
  }
  onClick() {
    this.setState({ isLiked: !this.state.isLiked });
  }
  convertDate(date) {
    const hoursZero = date.getHours() < 10 ? "0" : "";
    const minutesZero = date.getMinutes() < 10 ? "0" : "";
    return `${hoursZero + date.getHours()}:${minutesZero + date.getMinutes()}`;
  }
  convertDateForEditDate(date) {
    const mounthZero = date.getMonth() + 1 < 10 ? "0" : "";
    const dateZero = date.getDate() < 10 ? "0" : "";
    return `${this.convertDate(date)} ${dateZero + date.getDate()}.${
      mounthZero + (date.getMonth() + 1)
    }.${date.getFullYear()}`;
  }
  deleteMessage() {
    this.props.deleteMessage(this.props.message.id);
  }
  selectMessageForEdit() {
    this.props.openEditor(this.props.message.id, this.props.message.text);
  }
  render() {
    const p = this.props.message;
    let tools;
    let user = false;
    if (this.props.isOwn)
      tools = (
        <div className="tools">
          <i
            onClick={this.selectMessageForEdit}
            className="message-edit fas fa-pencil-alt"
          ></i>
          <i
            onClick={this.deleteMessage}
            className="fas fa-trash-alt message-delete"
          ></i>
        </div>
      );
    else {
      tools = (
        <div className="tools">
          <div className="byrger"></div>
          <i
            onClick={this.onClick}
            className={
              (this.state.isLiked ? "message-liked" : "message-like") +
              " fas fa-heart"
            }
          ></i>
        </div>
      );
      user = (
        <div className="user-message-wrp">
          <img
            className="message-user-avatar"
            src={p.avatar}
            alt={"user-img-" + p.user}
          />
          <div className="message-user-name">{p.user}</div>
        </div>
      );
    }

    let date = this.convertDate(new Date(p.createdAt));
    if (p.editedAt) {
      date = "edited at " + this.convertDateForEditDate(new Date(p.editedAt));
    }
    return (
      <div className={this.props.isOwn ? "own-message" : "message"}>
        <div className="message-wrp">
          {user}
          <div className="main-message-wrp">
            <p className="message-text">{p.text} </p>
            <div className="message-time">{date}</div>
          </div>
          {tools}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteMessage: (id) => dispatch(deleteMessage(id)),
    openEditor: (ID, text) => dispatch(openEditor(ID, text)),
  };
};

export default connect(null, mapDispatchToProps)(Message);
