import React from "react";
export default class DateLine extends React.Component {
  convertDate(date) {
    const dayMilliseconds = 24 * 60 * 60 * 1000;
    const today = new Date().setHours(0, 0, 0, 0);
    if (date > today) return "Today";
    else if (date > today - dayMilliseconds) return "Yesterday";
    else {
      const weekday = date.toLocaleString("en-DE", { weekday: "long" });
      const month = date.toLocaleString("en-DE", { month: "long" });
      const day = date.toLocaleString("en-DE", { day: "numeric" });
      return weekday + ", " + day + " " + month;
    }
  }
  render() {
    const date = this.convertDate(this.props.date);
    return (
      <div className="line">
        <div className="line-wrp">
          <hr />
        </div>
        <div className="messages-divider">{date}</div>
      </div>
    );
  }
}
