import React from "react";

export default class Header extends React.Component {
  convertDate(date) {
    const mounthZero = date.getMonth() + 1 < 10 ? "0" : "";
    const dateZero = date.getDate() < 10 ? "0" : "";
    const hoursZero = date.getHours() < 10 ? "0" : "";
    const minutesZero = date.getMinutes() < 10 ? "0" : "";
    return `${hoursZero + date.getHours()}:${minutesZero + date.getMinutes()} ${
      dateZero + date.getDate()
    }.${mounthZero + (date.getMonth() + 1)}.${date.getFullYear()}`;
  }
  render() {
    const messages = this.props.messages ?? [];
    const numMessage = messages.length ?? 0;
    let date = "about 65 million years ago, in the morning";
    if (numMessage !== 0)
      date = this.convertDate(new Date(messages[numMessage - 1].createdAt));
    const users = new Set();

    messages.forEach((m) => {
      users.add(m.userId);
    });
    const numUsers = users.size;
    return (
      <div className="header">
        <div className="header-title">{this.props.title}</div>
        <div className="header-users-count">{numUsers + " participants"} </div>
        <div className="header-messages-count">{numMessage + " messages"}</div>
        <div className="header-last-message-date">
          {"last message at   " + date}
        </div>
      </div>
    );
  }
}
