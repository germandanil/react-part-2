import React from "react";

import { connect } from "react-redux";
import { preloaderOff, setMessages, openEditor } from "../chatReducer/actions";

import Preloader from "./Preloader";
import Header from "./Header";
import MessageList from "./Messagelist";
import MessageInput from "./Input";
import EditForm from "./EditForm";

import { MY_ID } from "../myConfig";

const _ = require("lodash");

class Chat extends React.Component {
  constructor(props) {
    super(props);
    document
      .querySelector("body")
      .addEventListener("keyup", this.onArrowEditLastOwnMessage.bind(this));
  }
  getLastOwnMessage() {
    let message = null;
    this.props.messages.forEach((m) => {
      if (m.userId === MY_ID) message = m;
    });
    return message;
  }
  onArrowEditLastOwnMessage(event) {
    if (event.key === "ArrowUp") {
      let lastMessage = this.getLastOwnMessage(
        this.onArrowEditLastOwnMessage.bind(this)
      );
      if (lastMessage && !this.props.editModal) {
        this.props.openEditor(lastMessage.id, lastMessage.text);
      }
    }
  }
  componentDidMount() {
    fetch(this.props.url)
      .then((res) => res.json())
      .then((resArray) => {
        let messagesArray = resArray.map((m) => {
          m.liked = false;
          return m;
        });
        messagesArray = _.sortBy(messagesArray, [
          (m) => {
            return Date.parse(m.createdAt);
          },
        ]);
        this.props.setMessages(messagesArray);
        this.props.preloaderOff();
      });
  }
  render() {
    const preloaderShow = this.props.preloader;
    let content = [<Preloader key="Preloader" />];
    if (!preloaderShow) {
      content = [
        <EditForm key="EditForm" show={this.props.editModal} />,
        <Header
          key="Header"
          messages={this.props.messages}
          title="Hidden pull"
        />,
        <MessageList
          key="MessageList"
          messages={this.props.messages}
          ownID={MY_ID}
        />,
        <MessageInput key="MessageInput" />,
      ];
    }
    return <div className="chat">{content}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    preloader: state.chat.preloader,
    messages: state.chat.messages,
    editModal: state.chat.editModal,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    preloaderOff: () => dispatch(preloaderOff()),
    setMessages: (messagesArray) => dispatch(setMessages(messagesArray)),
    openEditor: (ID, text) => dispatch(openEditor(ID, text)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
