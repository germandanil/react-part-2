import React from "react";
export default class Preloader extends React.Component {
  render() {
    return (
      <div className="preloader">
        <i className="spinner fas fa-spinner fa-spin"></i>
      </div>
    );
  }
}
