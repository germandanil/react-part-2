import React from "react";
import Chat from "./components/chat";
import src from "./data.js";

import { Provider } from "react-redux";
import store from "./store";
function App() {
  return (
    <Provider store={store}>
      <Chat url={src} />
    </Provider>
  );
}

export default App;
